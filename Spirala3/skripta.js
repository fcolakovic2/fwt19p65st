function promijeni(k){
    var i=document.getElementById("slikanova"+k);   //uzima sve slike profesora i asistenata, bukvalno ide od slikanova1 do slikanova7 po ID-u

    if (k<7) {i.style.height='270px';}   //za svakog profesora se slika prosiruje tj mijenja joj se height kako bi se prosirila tako da zauzme cijeli svoj element. nije bilo potrebe dirati
    else{ i.style.height='230px';       // width jer je svaka slika na mojoj stranici vec rasirena dovoljno da zauzme cijeli svoj element, samo je trebalo produziti tj dodati height.
    i.style.paddingTop='0px';
    i.style.paddingRight='0px';
    i.style.paddingLeft='0px';
    i.style.paddingBottom='0px';}
    
    if (k==1){
            document.getElementById("ime1").style.display="none";
            document.getElementById("titula1").style.display="none";   //svaki od ovih 7 uslova radi isto, uzima profesora/asistenta tj. njegovu tabelu i sakriva sve podatke kao sto su ime, titula, brojkanc i link 
            document.getElementById("brojkanc1").style.display="none";   //kako bi se slika mogla fino prosiriti 
            document.getElementById("link1").style.display="none";

        
    }

    if (k==2){
        
        document.getElementById("ime2").style.display="none";
        document.getElementById("titula2").style.display="none";
        document.getElementById("brojkanc2").style.display="none";
        document.getElementById("link2").style.display="none";
        
    }

    if (k==3){
        
        document.getElementById("ime3").style.display="none";
        document.getElementById("titula3").style.display="none";
        document.getElementById("brojkanc3").style.display="none";
        document.getElementById("link3").style.display="none";
        
    }

    if (k==4){
        
        document.getElementById("ime4").style.display="none";
        document.getElementById("titula4").style.display="none";
        document.getElementById("brojkanc4").style.display="none";
        document.getElementById("link4").style.display="none";
        
    }

    if (k==5){
        
        document.getElementById("ime5").style.display="none";
        document.getElementById("brojkanc5").style.display="none";
        document.getElementById("link5").style.display="none";
        
    }

    if (k==6){
        
        document.getElementById("ime6").style.display="none";
        document.getElementById("brojkanc6").style.display="none";
        document.getElementById("link6").style.display="none";
        
    }

    if (k==7){
        
        document.getElementById("ime7").style.display="none";
        document.getElementById("brojkanc7").style.display="none";
        document.getElementById("link7").style.display="none";
    
    }
}



function oboji(x){
    
    if (x>4){
        document.getElementById("z"+x).style.backgroundColor="yellow";   // izracunao sam koliko ima kojih profesora, npr ovaj zadnji uslov predstavlja prof. Zeljka Jurica. On predaje 3 predmeta
    }                                                                    // po mojoj tabeli, tako da za svaki element iduci od y1 ( y1, y2 , y3 ) treba uzeti tj dohvatiti pomocu getElementbyID i obojiti mu
                                                                         //pozadinu u zuto. Ista logika je i za ostale profesore u narednim if-ovima tj. uslovima. Ostale profesore sam oznacavao sa "y" te davao im razlicite
    else if (x==4){                                                      //vrijednosti ipsilona od 1 do 13 jer mi je tako bilo lakse proci kroz petlju pomocu njih 
        for (var i=11;i<14;i++)
        document.getElementById("y"+i).style.backgroundColor="yellow";
    }

    else if (x==3){
        for (var i=8;i<11;i++)
        document.getElementById("y"+i).style.backgroundColor="yellow";
    }

    else if (x==2){
        for (var i=4;i<8;i++)
        document.getElementById("y"+i).style.backgroundColor="yellow";
    }
      
    else if (x==1){
       for (var i=1;i<4;i++)   
       document.getElementById("y"+i).style.backgroundColor="yellow";
   }
}


function sakrij(x){
    if (x==1) {
      document.getElementById("BWT").style.display="none";    //za prvu godinu sam stavio vrijednost x==1, tada se sakrivaju ova 2 predmeta druge godine tj. njihov display je "none". Ista logika je koristena i za
      document.getElementById("RS").style.display="none"; 
                                                              // drugu godinu, odvojio sam ih pomocu razlicitih vrijednosti x-a, x==1 odnosno x==2. A za prikaz svih predmeta koristio sam x==3 pri cemu nisam
    }                                                         // nista posebno dirao, vec samo postavio display na "initial" sto zapravo znaci da su vracene pocetne vrijednosti, a pocetna vrijednost je da se svi
    else if (x==2) {                                          //predmeti normalno prikazu. posljednih nekoliko linija u drugom uslovu , tj. mijenjanje gridRowa , je postavljena cisto radi estetike, da ne bi predmeti 
      document.getElementById("OBP").style.display="none";    // otisli na dno stranice kada se prikazu samo 2 predmeta druge godine, ali funkcija radi i bez njega.
      document.getElementById("MUR1").style.display="none";
      document.getElementById("UUP").style.display="none";
      document.getElementById("FWT").style.display="none";
      document.getElementById("RS").style.gridRow="2/3";
    }

    else if (x==3) {
      document.getElementById("OBP").style.display="initial";
      document.getElementById("MUR1").style.display="initial";
      document.getElementById("UUP").style.display="initial";
      document.getElementById("FWT").style.display="initial";
      document.getElementById("BWT").style.display="initial";
      document.getElementById("RS").style.display="initial";
      document.getElementById("RS").style.gridRow="initial";     
      document.getElementById("OBP").style.gridRow="initial";    //kada je x==3, koristi se dugme za prikaz svih predmeta. Style-display se stavlja na initial kao sto je objasnjeno gore, dok style.gridRow takodjer
      document.getElementById("MUR1").style.gridRow="initial";   // pored estetike, ima jos jednu funkciju. Naprimjer, ako klilknemo na sortiraj abecedno, predmeti ce se sortirati. gridRow="initial" za svaki predmet
      document.getElementById("UUP").style.gridRow="initial";  // osigurava da se kada se klikne onda na "svi predmeti" oni prikazu nesortirano, kao na pocetku .  To je implementirano jer sam nacuo da je potrebno
      document.getElementById("FWT").style.gridRow="initial";  // da dugme "svi predmeti" prikaze predmete nesortirane, onakve kakvi i jesu bili na samom pocetku.
      document.getElementById("BWT").style.gridRow="initial";
      document.getElementById("RS").style.gridRow="initial";



    }
}

function sortiraj(){
      document.getElementById("OBP").style.gridRow="4/5";   //funkcija je pozvana onClick metodom tj klikom na dugme "Sortiraj abecedno". Slicno je pozvana i prethodna funkcija tj. takodjer onClick metodom. 
      document.getElementById("MUR1").style.gridRow="3/4";  //Svi DIV-ovi predmeta su dohvaceni pomocu getElementById a zatim im je samo promijenjen raspored tj sortirani su abecedno po nazivu
      document.getElementById("UUP").style.gridRow="6/7";    //pomocu gridRow-a. Mogla je funkcija biti i duplo duza da smo koristili zasebno gridRowStart pa gridRowEnd za svaki DIV 
      document.getElementById("FWT").style.gridRow="2/3";    //ali je ovako krace i preglednije. 
      document.getElementById("BWT").style.gridRow="1/2";
      document.getElementById("RS").style.gridRow="5/6";
}

