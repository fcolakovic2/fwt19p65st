let assert = chai.assert;

  describe('Pretraga', function() {
  describe('pretragaPredmet()', function(){
   it('Trebaju biti prikazani svi predmeti', function(){
  Pretraga.pretragaPredmet("");
  let tabele = document.getElementById("tabela1");
  let tabela = tabele[tabele.length-1]
  let redovi = tabela.getElementsByTagName("tr");
  var broj=0;
  for (var i=0;i<redovi.length;i++){
    var sakriveno=window.getComputedStyle(redovi[i])
    if (sakriveno.display!=='none') broj++;
  }
  assert.equal(broj,20,"Broj redova treba biti 20");
  
       })
     })
   } 
  );

     describe('Pretraga', function(){
     describe('pretragaNastavnik()', function() {
        it('Ne treba biti prikazan nijedan predmet', function() {
             Pretraga.pretragaPredmet("test");
                let tabele = document.getElementById("tabela1");
                let tabela = tabele[tabele.length-1]
                let redovi = tabela.getElementsByTagName("tr");
                assert.equal(redovi.length, 0,"Broj redova treba biti 0");
                  })
     }) });
                    describe('Pretraga', function(){
                    describe('pretragaGodina()', function() {
                       it('Treba prikazati samo predmete druge godine', function() {
                            Pretraga.pretragaPredmet("2");
                               let tabele = document.getElementById("tabela1");
                               let tabela = tabele[tabele.length-1]
                               let redovi = tabela.getElementsByTagName("tr");
                               assert.equal(redovi.length, 7,"Broj redova treba biti 7");
                                 })
                    }) });;
                                
                    describe('Pretraga', function(){
                      describe('pretragaNastavnik()', function() {
                         it('Treba prikazati samo predmete prof. Samira', function() {
                              Pretraga.pretragaPredmet("Samir Ribic");
                                 let tabele = document.getElementById("tabela1");
                                 let tabela = tabele[tabele.length-1]
                                 let redovi = tabela.getElementsByTagName("tr");
                                 assert.equal(redovi.length, 5,"Broj redova treba biti 5");
                                   })
                      }) });





                                 describe('Pretraga', function(){
                                 describe('pretragaGodina()', function() {
                                    it('Treba prikazati samo predmete prve godine', function() {
                                         Pretraga.pretragaPredmet("1");
                                            let tabele = document.getElementById("tabela1");
                                            let tabela = tabele[tabele.length-1]
                                            let redovi = tabela.getElementsByTagName("tr");
                                            assert.equal(redovi.length, 13,"Broj redova treba biti 13");
                                              })
                                 }) });
                                              
                                              describe('Pretraga', function(){
                                              describe('pretragaNastavnik()', function() {
                                                it('Treba prikazati 3 predmeta prof. Vedrana', function() {
                                                     Pretraga.pretragaPredmet("Vedran");
                                                        let tabele = document.getElementById("tabela1");
                                                        let tabela = tabele[tabele.length-1]
                                                        let redovi = tabela.getElementsByTagName("tr");
                                                        assert.equal(redovi.length, 5,"Broj redova treba biti 5");
                                                          })
                                              }) });
                                                          
                                                          describe('Pretraga', function(){
                                                          describe('pretragaNastavnik()', function() {
                                                            it('Treba prikazati 1 predmet prof. Amile', function() {
                                                                 Pretraga.pretragaPredmet("Amila");
                                                                    let tabele = document.getElementById("tabela1");
                                                                    let tabela = tabele[tabele.length-1]
                                                                    let redovi = tabela.getElementsByTagName("tr");
                                                                    assert.equal(redovi.length, 3,"Broj redova treba biti 3");
                                                                      })
                                                          }) });
                                                                     describe('Pretraga', function(){
                                                                      describe('pretragaPredmet()', function() {
                                                                        it('Samo je jedan FWT', function() {
                                                                            Pretraga.pretragaPredmet("fwt");
                                                                            let tabele = document.getElementById("tabela1");
                                                                            let tabela = tabele[tabele.length-1]
                                                                            let redovi = tabela.getElementsByTagName("tr");
                                                                            assert.equal(redovi.length, 1,"Broj redova treba biti 1");
                                                                          })
                                                                      }) }); 
                                                                    